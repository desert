/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert.ui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Component;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;

import net.sourceforge.desert.ImageFilter;
import net.sourceforge.desert.ImageFilterClearImplementation;
import net.sourceforge.desert.ImageFilterFadeImplementation;
import net.sourceforge.desert.Particle;
import net.sourceforge.desert.ParticleEngine;
import net.sourceforge.desert.Utilities;

/**
 *
 * @author codistmonk (creation 2010-04-15)
 */
@SuppressWarnings("serial")
public class DesertPanel extends Panel {

    private final Timer timer;

    private final DrawingBoard drawingBoard;

    public DesertPanel() {
        super(new BorderLayout());
        this.timer = new Timer(FRAMERATE);
        this.drawingBoard = new DrawingBoard();

        final CursorPositionLabel xyMouse = new CursorPositionLabel();
        final FramerateLabel framerateLabel = new FramerateLabel();
        final ParticleCountLabel particleCountLabel = this.new ParticleCountLabel();
        final Checkbox pauseButton = new Checkbox("Pause");
        final Button resetButton = new Button("Reset");

        this.add(this.getDrawingBoard(), BorderLayout.CENTER);
        this.add(verticalBox(
                horizontalBox(framerateLabel, particleCountLabel, xyMouse),
                horizontalBox(resetButton, pauseButton),
                horizontalBox(new Label("Particle type: "), this.new ParticleTypeChoice()),
                horizontalBox(new Label("Image filter: "), this.new ImageFilterChoice()),
                horizontalBox(new Label("Brush mode: "), this.new BrushModeChoice(), new Label("Brush shape: "), this.new BrushChoice())
        ), BorderLayout.SOUTH);

        pauseButton.addItemListener(this.new Pause());
        resetButton.addActionListener(this.new Reset());

        this.getTimer().addActionListener(this.getDrawingBoard());
        this.getDrawingBoard().addMouseMotionListener(xyMouse);
        this.getDrawingBoard().setParticleEngine(new ParticleEngine());
        this.getTimer().addActionListener(framerateLabel);
        this.getTimer().addActionListener(particleCountLabel);
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final Timer getTimer() {
        return this.timer;
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final DrawingBoard getDrawingBoard() {
        return this.drawingBoard;
    }

    /**
     *
     * @author codistmonk (creation 2010-04-20)
     */
    private final class Reset implements ActionListener {

        @Override
        public final void actionPerformed(final ActionEvent event) {
            DesertPanel.this.getDrawingBoard().getParticleEngine().removeAllParticles();
        }

    }

    /**
     *
     * @author codistmonk (creation 2010-04-20)
     */
    private final class Pause implements ItemListener {

        @Override
        public final void itemStateChanged(final ItemEvent event) {
            DesertPanel.this.getDrawingBoard().setPaused(event.getStateChange() == ItemEvent.SELECTED);
        }

    }

    /**
     *
     * @author codistmonk (creation 2010-04-21)
     */
    @SuppressWarnings("serial")
    private final class ImageFilterChoice extends Choice {

        public ImageFilterChoice() {
            this.add("Clear");
            this.add("Fade");

            this.addItemListener(this.new ItemHandler());
        }

        /**
         *
         * @author codistmonk (creation 2010-04-17)
         */
        private final class ItemHandler implements ItemListener {

            @Override
            public final void itemStateChanged(final ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    try {
                        DesertPanel.this.getDrawingBoard().setImageFilter(this.createImageFilter());
                    } catch (final InstantiationException exception) {
                        Logger.getLogger(DesertPanel.class.getName()).log(Level.SEVERE, null, exception);
                    } catch (final IllegalAccessException exception) {
                        Logger.getLogger(DesertPanel.class.getName()).log(Level.SEVERE, null, exception);
                    }
                }
            }

            /**
            * @return
            * <br>A new value
            * <br>A non-null value
            * @throws InstantiationException
            * @throws IllegalAccessException
            */
            private final ImageFilter createImageFilter() throws InstantiationException, IllegalAccessException {
                return (ImageFilter) new Class<?>[] {
                            ImageFilterClearImplementation.class,
                            ImageFilterFadeImplementation.class
                }[ImageFilterChoice.this.getSelectedIndex()].newInstance();
            }

        }

    }

    /**
     *
     * @author andrew (creation 2010-04-17)
     */
    @SuppressWarnings("serial")
    private final class ParticleTypeChoice extends Choice {

        public ParticleTypeChoice() {
            final ResourceBundle resourceBundle = ResourceBundle.getBundle(Utilities.RESOURCE_BASE + "messages");

            for (final Particle.Type type : Particle.Type.values()) {
                this.addItem(resourceBundle.getString(type.toString()));
            }

            this.updateDrawingBoardParticleType();
            
            this.addItemListener(this.new ItemHandler());
        }

        final void updateDrawingBoardParticleType() {
            DesertPanel.this.getDrawingBoard().setParticleType(
                    Particle.Type.values()[ParticleTypeChoice.this.getSelectedIndex()]);
        }

        /**
         *
         * @author andrew (creation 2010-04-17)
         */
        private final class ItemHandler implements ItemListener {

            @Override
            public final void itemStateChanged(final ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    ParticleTypeChoice.this.updateDrawingBoardParticleType();
                }
            }

        }

    }

    /**
     *
     * @author codistmonk (creation 2010-04-28)
     */
    @SuppressWarnings("serial")
    private final class BrushModeChoice extends Choice {

        public BrushModeChoice() {
            this.addItem("Add");
            this.addItem("Erase");

            this.updateDrawingBoardBrushMode();

            this.addItemListener(this.new ItemHandler());
        }

        final void updateDrawingBoardBrushMode() {
            DesertPanel.this.getDrawingBoard().setBrushMode(
                    DrawingBoard.BrushMode.values()[BrushModeChoice.this.getSelectedIndex()]);
        }

        /**
         *
         * @author andrew (creation 2010-04-17)
         */
        private final class ItemHandler implements ItemListener {

            @Override
            public final void itemStateChanged(final ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    BrushModeChoice.this.updateDrawingBoardBrushMode();
                }
            }

        }

    }

    /**
     *
     * @author codistmonk (creation 2010-04-25)
     */
    @SuppressWarnings("serial")
    private final class BrushChoice extends Choice {

        private final List<String> brushPaths;
        
        public BrushChoice() {
            this.brushPaths = Utilities.listBrushPaths();

            for (final String brushPath : this.brushPaths) {
                this.addItem(getUserReadableBrushName(brushPath));
            }

            this.updateDrawingBoardBrush();

            this.addItemListener(this.new ItemHandler());
        }

        final void updateDrawingBoardBrush() {
            DesertPanel.this.getDrawingBoard().setBrush(
                    Utilities.getBrush(this.brushPaths.get(BrushChoice.this.getSelectedIndex())));
        }

        /**
         *
         * @author condistmonk (creation 2010-04-25)
         */
        private final class ItemHandler implements ItemListener {

            @Override
            public final void itemStateChanged(final ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    BrushChoice.this.updateDrawingBoardBrush();
                }
            }

        }

    }

    /**
     *
     * @author codistmonk (creation 2010-04-15)
     */
    @SuppressWarnings("serial")
    private final class ParticleCountLabel extends Label implements ActionListener {

        @Override
        public final void actionPerformed(final ActionEvent event) {
            this.setText("Particle count: " + DesertPanel.this.getDrawingBoard().getParticleCount());
        }

    }

    /**
     *
     * @author andrew (creation 2010-04-19)
     */
    @SuppressWarnings("serial")
    private final class CursorPositionLabel extends Label implements MouseMotionListener {

        @Override
        public final void mouseMoved(final MouseEvent event) {
            this.updateText(event);
        }

        @Override
        public final void mouseDragged(final MouseEvent event) {
            this.updateText(event);
        }

        /**
         *
         * @param event
         * <br>Should not be null
         */
        private final void updateText(final MouseEvent event) {
            this.setText("(" + event.getX() + ", " +
                    (DesertPanel.this.getDrawingBoard().getHeight() - event.getY() - 1) + ")");
        }

    }

    public static final int FRAMERATE = 25;

    /**
     *
     * @param components
     * <br> Should not be null
     * @return
     * <br>A new value
     * <br>A non-null value
     */
    private static final Box horizontalBox(final Component... components) {
        // TODO do not use Swing
        final Box result = Box.createHorizontalBox();

        for (final Component component : components) {
            result.add(component);
        }

        return result;
    }

    /**
     *
     * @param components
     * <br> Should not be null
     * @return
     * <br>A new value
     * <br>A non-null value
     */
    private static final Box verticalBox(final Component... components) {
        // TODO do not use Swing
        final Box result = Box.createVerticalBox();

        for (final Component component : components) {
            result.add(component);
        }

        return result;
    }

    /**
     * Transforms the brush resource path into a user-readable form.
     * <br>Eg: "org/sourceforge/desert/resources/images/dot_1x1.png" becomes "Dot 1x1"
     * @param brushPath
     * <br>Should not be null
     * @return
     * <br>A non-null value
     * <br>A new value
     */
    static final String getUserReadableBrushName(final String brushPath) {
        final String result = Utilities.getResourceName(brushPath).split("brush_")[1].split("\\.")[0].replaceAll("_", " ");

        return Character.toUpperCase(result.charAt(0)) + result.substring(1);
    }

    /**
     *
     * @author codistmonk (creation 2010-04-15)
     */
    @SuppressWarnings("serial")
    private static final class FramerateLabel extends Label implements ActionListener {

        private double meanFramerate;

        @Override
        public final void actionPerformed(final ActionEvent event) {
            this.meanFramerate = 0.9F * this.meanFramerate + 0.1F * 1.0 / ((Timer.Event) event).getDeltaTime();
            this.setText("Framerate: " + String.format("%.1f", this.meanFramerate));
        }

    }
}
