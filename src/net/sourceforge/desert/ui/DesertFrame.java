/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert.ui;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author codistmonk (creation 2010-04-16)
 */
@SuppressWarnings("serial")
class DesertFrame extends Frame {

    private final DesertPanel desertPanel;

    public DesertFrame() {
        this.desertPanel = new DesertPanel();

        this.getDesertPanel().getDrawingBoard().setPreferredSize(new Dimension(DRAWING_BOARD_PREFERRED_WIDTH, DRAWING_BOARD_PREFERRED_HEIGHT));
        this.add(this.getDesertPanel());
        
        this.addWindowListener(new ApplicationCloser());
        this.pack();
        this.setLocationRelativeTo(null);
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final DesertPanel getDesertPanel() {
        return desertPanel;
    }

    public static final int DRAWING_BOARD_PREFERRED_WIDTH = 400;

    public static final int DRAWING_BOARD_PREFERRED_HEIGHT = 300;

    /**
     *
     * @author codistmonk (creation 2010-04-16)
     */
    private static class ApplicationCloser extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }

    }

}
