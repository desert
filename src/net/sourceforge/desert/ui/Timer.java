/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package net.sourceforge.desert.ui;

import java.awt.event.ActionEvent;

/**
 *
 * @author codistmonk (creation 2010-04-15)
 */
@SuppressWarnings("serial")
public class Timer extends javax.swing.Timer {

    private long time;

    /**
     *
     * @param framerate
     * <br>Should not be null
     * <br>Range: <code>[1 .. Integer.MAX_VALUE]</code>
     */
    public Timer(final Integer framerate) {
        super(1000 / framerate, null);
    }

    @Override
    protected final void fireActionPerformed(final ActionEvent event) {
        super.fireActionPerformed(new Event(event, this.computeDeltaTime()));
    }

    /**
     *
     * @return the time elapsed since the last call in seconds
     * <br>A non-null value
     * <br>Range: <code>[0.0 .. Long.MAX_VALUE / 1000.0]</code>
     */
    private final float computeDeltaTime() {
        final long newTime = System.currentTimeMillis();

        if (this.time == 0L) {
            this.time = newTime - this.getDelay();
        }

        final float result = (newTime - this.time) / 1000F;

        this.time = newTime;

        return result;
    }

    /**
     *
     * @author codistmonk (creation 2010-04-15)
     */
    @SuppressWarnings("serial")
    public static final class Event extends ActionEvent {

        private final float deltaTime;

        /**
         *
         * @param timerEvent the original swing timer event
         * <br>Should not be null
         * @param deltaTime
         * <br>Should not be null
         * <br>Range: <code>[0 .. float.POSITIVE_INFINITY[</code>
         */
        Event(final ActionEvent timerEvent, final float deltaTime) {
            super(timerEvent.getSource(), timerEvent.getID(), timerEvent.getActionCommand(), timerEvent.getWhen(), timerEvent.getModifiers());
            this.deltaTime = deltaTime;
        }

        /**
         *
         * @return
         * <br>A non-null value
         * <br>Range: <code>[0 .. float.POSITIVE_INFINITY[</code>
         */
        public float getDeltaTime() {
            return this.deltaTime;
        }

    }

}
