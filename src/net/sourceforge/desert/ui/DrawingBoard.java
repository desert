/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert.ui;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Iterator;

import net.sourceforge.desert.ImageFilter;
import net.sourceforge.desert.ImageFilterClearImplementation;
import net.sourceforge.desert.Particle;
import net.sourceforge.desert.ParticleEngine;
import net.sourceforge.desert.ParticleRenderer;
import net.sourceforge.desert.Utilities;

/**
 * The drawing board, extending the graphics Canvas.
 * @author codistmonk (creation 2010-04-13)
 */
@SuppressWarnings("serial")
public class DrawingBoard extends Canvas implements ActionListener {

    private final ParticleRenderer particleRenderer;

    private ParticleEngine particleEngine;

    private Particle.Type particleType;

    private BufferedImage buffer;

    private ImageFilter imageFilter;

    private boolean paused;

    /**
     * This mask covers the board and is used to tell where particles can be added.
     * <br>It should be updated at the same time as the drawing buffer.
     */
    private boolean[][] particleMask;

    private boolean[][] brush;

    private BrushMode brushMode;

    public DrawingBoard() {
        this.particleRenderer = new ParticleRenderer();
        this.particleEngine = new ParticleEngine();
        this.particleType = Particle.Type.values()[0];
        this.imageFilter = new ImageFilterClearImplementation();
        this.brush = Utilities.getBrush(Utilities.listBrushPaths().get(0));
        this.brushMode = BrushMode.ADD;

        this.addComponentListener(this.new ResizeHandler());

        final MouseHandler mouseHandler = this.new MouseHandler();

        this.addMouseListener(mouseHandler);
        this.addMouseMotionListener(mouseHandler);

        // This doesn't seem of much use in applets
        this.setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
    }

    @Override
    public final void actionPerformed(final ActionEvent event) {
        if (!this.isPaused()) {
            this.getParticleEngine().update(((Timer.Event) event).getDeltaTime());
        }

        this.repaint();
    }

    @Override
    public final void paint(final Graphics g) {
        if (this.buffer != null) {
            this.clearBuffer();

            this.drawParticlesToBuffer();

            this.drawBuffer(g);
        }
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final boolean[][] getBrush() {
        return brush;
    }

    /**
     *
     * @param brush
     * <br>Should not be null
     * <br>Reference parameter
     */
    public final void setBrush(final boolean[][] brush) {
        this.brush = brush;
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final BrushMode getBrushMode() {
        return this.brushMode;
    }

    /**
     *
     * @param brushMode
     * <br>Should not be null
     * <br>Reference parameter
     */
    public final void setBrushMode(final BrushMode brushMode) {
        this.brushMode = brushMode;
    }

    /**
     * 
     * @return
     */
    public boolean isPaused() {
        return this.paused;
    }

    /**
     *
     * @param paused
     */
    public void setPaused(final boolean paused) {
        this.paused = paused;
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final ImageFilter getImageFilter() {
        return this.imageFilter;
    }

    /**
     *
     * @param filter
     * <br>Should not be null
     * <br>Reference parameter
     */
    public final void setImageFilter(final ImageFilter filter) {
        this.imageFilter = filter;
    }

    /**
     *
     * @return
     * <br>Range: <code>[0 .. Integer.MAX_VALUE]</code>
     */
    public final int getParticleCount() {
        return this.getParticleEngine().getParticleCount();
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    public final ParticleEngine getParticleEngine() {
        return this.particleEngine;
    }

    /**
     *
     * @param particleEngine
     * <br>Should not be null
     * <br>Reference parameter
     */
    public final void setParticleEngine(final ParticleEngine particleEngine) {
        if (particleEngine != this.getParticleEngine()) {
            for (final Particle particle : this.getParticleEngine()) {
                particleEngine.addParticle(particle.getType(), particle.getX(), particle.getY(), particle.getSpeedX(), particle.getSpeedY(), particle.getMass(), particle.getTemp());
            }

            this.particleEngine = particleEngine;

            this.getParticleEngine().setBoardSize(this.getSize());
        }
    }

    /**
     *
     * @return
     * <br>A non-null value
     */
    public final Particle.Type getParticleType() {
        return this.particleType;
    }

    /**
     *
     * @param type
     * <br>Should not be null
     */
    public final void setParticleType(final Particle.Type type) {
        this.particleType = type;
    }

    /**
     * Add a new particle
     * @param x
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param y
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param speedX
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param speedY
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param mass
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     */
    final void addParticle(final float x, final float y, final float speedX, final float speedY, final float mass, final Integer temp) {
        this.getParticleEngine().addParticle(this.getParticleType(), x, y, speedX, speedY, mass, temp);
    }

    final void resizeBuffer() {
        if (this.getWidth() >0 && this.getHeight() > 0) {
            this.buffer = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
            this.particleMask = new boolean[this.getHeight()][this.getWidth()];
            this.getParticleEngine().setBoardSize(this.getSize());
            this.particleRenderer.setBuffer(this.buffer);
        }
        else {
            this.buffer = null;
        }
    }

    /**
     *
     * @param x
     * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]</code>
     * @param y
     * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]</code>
     * @return <code>true</code> if the specified location is outside the board or contain no particle
     */
    final boolean canAddParticle(final int x, final int y) {
        return this.isInsideBoard(x, y) && !this.particleMask[y][x];
    }

    private final void clearBuffer() {
        this.buffer = this.imageFilter.filter(this.buffer);

        for (int i = 0; i < this.particleMask.length; ++i) {
            Arrays.fill(this.particleMask[i], false);
        }

        for (final Particle particle : this.getParticleEngine()) {
            if (this.isInsideBoard((int) particle.getX(), (int) particle.getY())) {
                this.particleMask[(int) particle.getY()][(int) particle.getX()] = true;
            }
        }
        
        this.particleRenderer.setBuffer(this.buffer);
    }

    private final void drawParticlesToBuffer() {
        for (final Particle particle : this.getParticleEngine()) {
            this.particleRenderer.draw(particle);
            
            if (this.isInsideBoard((int) particle.getX(), (int) particle.getY())) {
                this.particleMask[(int) particle.getY()][(int) particle.getX()] = true;
            }
        }
    }

    /**
     *
     * @param x
     * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]</code>
     * @param y
     * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]</code>
     * @return
     */
    private final boolean isInsideBoard(final int x, final int y) {
        return 0 <= x && x < this.getWidth() && 0 <= y && y < this.getHeight();
    }

    /**
     *
     * @param graphics
     * <br>Should not be null
     */
    private final void drawBuffer(final Graphics graphics) {
        graphics.drawImage(this.buffer, 0, 0, null);
    }

    /**
     *
     * @author codistmonk (creation 2010-04-13)
     */
    private final class ResizeHandler extends ComponentAdapter {

        @Override
        public final void componentResized(final ComponentEvent event) {
            DrawingBoard.this.resizeBuffer();
        }

    }

    /**
     * The mouse handler adds particles to the board when the user presses a mouse button or performs a drag gesture.
     * @author codistmonk (creation 2010-04-13)
     */
    private final class MouseHandler extends MouseAdapter {

        // TODO: Use triggers set off by classes called Desert*Element

        private Point lastPoint;

        @Override
        public final void mouseDragged(final MouseEvent event) {
            if (this.lastPoint != null) {
                this.addOrRemoveParticleStreak(this.lastPoint, event.getPoint());
            }
            else {
                this.addOrRemoveParticles(event.getX(), event.getY());
            }

            this.lastPoint = event.getPoint();
        }

        @Override
        public final void mousePressed(final MouseEvent event) {
            this.addOrRemoveParticles(event.getX(), event.getY());
            
            this.lastPoint = null;
        }

        /**
         *
         * @param origin
         * <br>Should not be null
         * @param destination
         * <br>Should not be null
         */
        private final void addOrRemoveParticleStreak(final Point origin, final Point destination) {
            float deltaX = destination.x - origin.x;
            float deltaY = destination.y - origin.y;
            final float pixelCount = Math.max(Math.abs(deltaX), Math.abs(deltaY));

            if (pixelCount > 0) {
                deltaX /= pixelCount;
                deltaY /= pixelCount;

                float x = origin.x;
                float y = origin.y;

                for (int i = 0; i < pixelCount; ++i) {
                    this.addOrRemoveParticles((int) x, (int) y);

                    x += deltaX;
                    y += deltaY;
                }
            }
        }

        /**
         * There is no real limit on <code>x</code> and <code>y</code> because
         * particles can be created outside the board and moved inside by the engines.
         * @param x
         * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]]</code>
         * @param y
         * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]]</code>
         */
        private final void addOrRemoveParticles(final int x, final int y) {
            switch (DrawingBoard.this.getBrushMode()) {
                case ADD:
                    this.addParticles(x, y);
                    break;
                case REMOVE:
                    this.removeParticles(x, y);
                    break;
            }
        }

        /**
         *
         * @param x
         * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]]</code>
         * @param y
         * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]]</code>
         */
        private final void addParticles(final int x, final int y) {
            final boolean[][] brush = DrawingBoard.this.getBrush();
            final int halfWidth = brush[0].length / 2;
            final int halfHeight = brush.length / 2;

            for (int i = 0; i < brush.length; ++i) {
                for (int j = 0; j < brush[0].length; ++j) {
                    final int x2 = x - halfWidth + j;
                    final int y2 = DrawingBoard.this.getHeight() - y + halfHeight - i;
                    
                    if (brush[i][j] && DrawingBoard.this.canAddParticle(x2, y2)) {
                        DrawingBoard.this.particleMask[y2][x2] = true;
                        DrawingBoard.this.addParticle(jitter(x2), jitter(y2), 0F, 0F, jitter(DrawingBoard.this.getParticleType().getMass()), DrawingBoard.this.getParticleType().getTemp());
                    }
                }
            }
        }

        /**
         * 
         * @param x
         * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]]</code>
         * @param y
         * <br>Range: <code>[Integer.MIN_VALUE .. Integer.MAX_VALUE]]</code>
         */
        private final void removeParticles(final int x, final int y) {
            final boolean[][] brush = DrawingBoard.this.getBrush();
            final int halfWidth = brush[0].length / 2;
            final int halfHeight = brush.length / 2;

            for (int i = 0; i < brush.length; ++i) {
                for (int j = 0; j < brush[0].length; ++j) {
                    if (brush[i][j]) {
                        final int x2 = x - halfWidth + j;
                        final int y2 = DrawingBoard.this.getHeight() - y + halfHeight - i;

                        for (final Iterator<Particle> iterator = DrawingBoard.this.getParticleEngine().iterator(); iterator.hasNext();) {
                            final Particle particle = iterator.next();

                            if (x2 == (int) particle.getX() && y2 ==(int) particle.getY()) {
                                iterator.remove();
                            }
                        }
                    }
                }
            }
        }

    }

    public static final int PREFERRED_WIDTH = 400;

    public static final int PREFERRED_HEIGHT = 300;

    /**
     * Use this function so that particle coordinates aren't perfect integers.
     * <br>having perfect integers can restrict the particles to vertical motion if
     * the only applied force is gravity.
     * @param value
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    private static final float jitter(final float value) {
        return value + (float) Math.random();
    }

    /**
     *
     * @author codistmonk (creation 2010-04-28)
     */
    public static enum BrushMode {

        ADD, REMOVE;

    }

}
