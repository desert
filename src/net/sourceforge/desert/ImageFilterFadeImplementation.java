/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert;

import java.awt.image.BufferedImage;

/**
 *
 * @author codistmonk (2010-04-21)
 */
public class ImageFilterFadeImplementation implements ImageFilter {

    // TODO see if the image filtering API in AWT can be used to improve performances

    @Override
    public final BufferedImage filter(final BufferedImage source) {
        for (int y = 0; y < source.getHeight(); ++y) {
            for (int x = 0; x < source.getWidth(); ++x) {
                source.setRGB(x, y, scaleARGB(PERSISTENCE, source.getRGB(x, y)));
            }
        }

        return source;
    }

    /**
     * Scales individually the components red, green and blue, and sets the alpha
     * component to <code>0xFF</code>.
     * @param scale
     * <br>Range: <code>[0F .. 255F]</code>
     * @param argb
     * <br>Range: <code>[0x00000000 .. 0xFFFFFFFF]</code>
     * @return
     * <br>Range: <code>[0x00000000 .. 0xFFFFFFFF]</code>
     */
    private static final int scaleARGB(final float scale, final int argb) {
        final int red = ((int) ((argb & RED_MASK) * scale)) & RED_MASK;
        final int green = ((int) ((argb & GREEN_MASK) * scale)) & GREEN_MASK;
        final int blue = ((int) ((argb & BLUE_MASK) * scale)) & BLUE_MASK;

        return ALPHA_MASK + red + green + blue;
    }

    /**
     * 0F means no persistence, like clearing the image.
     * <br>1F means infinite persistence, like never clearing the image.
     */
    private static final float PERSISTENCE = 0.95F;

    private static final int ALPHA_MASK = 0xFF000000;

    private static final int RED_MASK   = 0x00FF0000;

    private static final int GREEN_MASK = 0x0000FF00;

    private static final int BLUE_MASK  = 0x000000FF;

}
