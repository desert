/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert;

import static java.lang.Math.*;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import net.sourceforge.desert.Particle.Type;

/**
 * A particle engine is an object responsible for the creation, storage, enumeration and update of particles.
 * <br>Warning: for engines that don't use Particle internally, the recommended way to implement
 * iteration is is to always return the same particle object updated for each actual particle.
 * <br>This is to avoid creating and destroying temporary objects, thus improving memory usage and speed.
 * <br>Therefore, when iterating over a ParticleEngine, users shouldn't expect a particle to be a different
 * object than the previous one (only the state might change).
 * <br>The same caution is required with the iterator objects: concurrent iteration is generally not supported.
 * @author codistmonk (creation 2010-04-17)
 */
public class ParticleEngine implements Iterable<Particle> {

    private final List<Particle> particles;

    private float gravity;

    private Dimension boardSize;

    private long newParticleId;

    private ParticleCellularImplementation[][] cells;

    private final ParticleCellularImplementation borderParticle;

    /**
     * This variable is part of a temporary measure to detect particle deletion.
     * <br>TODO remove when deletion is handled correctly
     */
    private int lastParticleCount;

    public ParticleEngine() {
        this.particles = new ArrayList<Particle>();
        this.newParticleId = Long.MIN_VALUE;
        this.borderParticle = this.new ParticleCellularImplementation(Particle.Type.IMMOBILE,
                Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, 0F, 0F, 0);

        this.setGravity(MAXIMUM_SPEED * signum(DEFAULT_GRAVITY));
    }

    /**
     *
     * @return a non-null value iff bounds are defined
     * <br>A possibly null value
     * <br>Can be a reference
     */
    public final Dimension getBoardSize() {
        return this.boardSize;
    }

    /**
     * Sets the size of the rectangle enclosing the board and thus limiting the movements of the particles;
     * if it is <code>null</code> then these limits are removed and the particle are free to go outside of the board.
     * <br>If the size is set to a non-null value while some particles are outside the board,
     * then these particles should be brought back inside in a limited amount of time.
     * @param size 
     * <br>Can be null
     * <br>Can be a reference parameter
     * <br>Can be a new value
     */
    public final void setBoardSize(final Dimension size) {
        this.boardSize = size;
        this.boardSizeChanged();
    }

    /**
     *
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public final float getGravity() {
        return this.gravity;
    }

    /**
     * The gravity is vertical.
     * <br>Positive values are upward.
     * @param gravity
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public final void setGravity(final float gravity) {
        this.gravity = gravity;
    }

    public final void removeAllParticles() {
        this.particles.clear();
    }

    /**
     *
     * @param type
     * <br>Should not be null
     * @param x
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @param y
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @param speedX
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @param speedY
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public final void addParticle(final Particle.Type type, final float x, final float y,
            final float speedX, final float speedY, final float mass, final Integer temp) {
        final ParticleDefaultImplementation particle = this.createParticle(type, x, y, speedX, speedY, temp);

        this.particles.add(particle);
        this.particleAdded(particle);
    }

    /**
     *
     * @return
     * <br>Range: <code>[0 .. Integer.MAX_VALUE]</code>
     */
    public final int getParticleCount() {
        return this.particles.size();
    }

    @Override
    public final Iterator<Particle> iterator() {
        return ((Iterable<Particle>) this.particles).iterator();
    }

    /**
     * Updates the positions and speeds of all the particles.
     * @param deltaTime in seconds
     * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
     */
    public final void update(float deltaTime) {
        this.updateCells();

        this.applyForces(deltaTime);

        this.updatePositions(deltaTime);
    }

    /**
     * The default implementation returns an instance of ParticleDefaultImplementation.
     * @param type
     * <br>Should not be null
     * @param x
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @param y
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @param speedX
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @param speedY
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     * @return
     * <br>A non-null value
     * <br>A new value
     */
    protected final ParticleDefaultImplementation createParticle(final Particle.Type type, final float x, final float y,
            final float speedX, final float speedY, final Integer temp) {
        return new ParticleCellularImplementation(type, x, y, speedX, speedY, temp);
    }

    /**
     * This method should be overriden if a special action must be executed just
     * after a particle has been added.
     * @param particle
     * <br>Should not be null
     * <br>Reference parameter
     */
    protected final void particleAdded(final ParticleDefaultImplementation particle) {
        this.setCell(particle.getX(), particle.getY(), (ParticleCellularImplementation) particle);
    }

    /**
     * This method should be overriden if a special action must be executed just
     * after the board size has been changed.
     */
    protected final void boardSizeChanged() {
        if (this.getBoardSize() != null && this.getBoardSize().width > 0 && this.getBoardSize().height > 0) {
            this.cells = new ParticleCellularImplementation[this.getBoardSize().width][this.getBoardSize().height];

            this.fillCells();
        } else {
            this.cells = null;
        }
    }

    /**
     * Constrains the particle inside the bounding rectangle if it exists.
     * @param particle
     * <br>Should not be null
     * <br>Input-output parameter
     */
    protected final void constrainPosition(final ParticleDefaultImplementation particle) {
        if (this.getBoardSize() != null) {
            // Left bound
            if (particle.getX() < 0F) {
                particle.setX(0F);
                particle.setSpeedX(abs(particle.getSpeedX()));
            }
            // Top bound
            if (particle.getY() < 0F) {
                particle.setY(0F);
                particle.setSpeedY(abs(particle.getSpeedY()));
            }
            // Right bound
            if (particle.getX() >= this.getBoardSize().width) {
                particle.setX(this.getBoardSize().width - 1F);
                particle.setSpeedX(-abs(particle.getSpeedX()));
            }
            // Bottom bound
            if (particle.getY() >= this.getBoardSize().height) {
                particle.setY(this.getBoardSize().height - 1F);
                particle.setSpeedY(-abs(particle.getSpeedY()));
            }
        }
    }

    /**
     *
     * @return
     * <br>Range: any long
     */
    final long generateNewId() {
        return this.newParticleId++;
    }

    /**
     * Does nothing if <code>x</code> and <code>y</code> do not locate a valid cell
     * that contains no particle or a mobile particle.
     * @param x
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param y
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param particle
     * <br>Can be null
     * <br>Reference parameter
     */
    final void setCell(final float x, final float y, final ParticleCellularImplementation particle) {
        assert particle == null || (int) x == (int) particle.getX() : x + " " + y + " " + particle;
        assert particle == null || (int) y == (int) particle.getY() : x + " " + y + " " + particle;

        if (this.isInGrid(x, y) &&
                (this.cells[(int) x][(int) y] == null || this.cells[(int) x][(int) y].getType().isMobile())) {
            this.cells[(int) x][(int) y] = particle;
        }
    }

    /**
     * TODO add more elements to act more earth-like
     * @param deltaTime in seconds
     * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
     */
    private void applyForces(final float deltaTime) {
        for (final ParticleCellularImplementation particle : this.getParticles()) {
            if (particle.getType().isMobile()) {
                if (particle.getType() == Particle.Type.WATER || particle.getType() == Particle.Type.LAVA ||
                        particle.getType() == Particle.Type.SAND || particle.getType() == Particle.Type.OIL) {
                    if (particle.getSpeedX() == 0F && particle.getSpeedY() == 0F) {
                        final float direction = (random() * 4) > 2 ? 1F : -1F;

                        if (this.isCellTraversable(particle.getX() + direction, particle.getY(), particle)) {
                            particle.setSpeedX(direction * MAXIMUM_SPEED * (0.50F + (float) random()) - particle.getMass());
                        }
                        else if (this.isCellTraversable(particle.getX() - direction, particle.getY(), particle)) {
                            particle.setSpeedX(-direction * MAXIMUM_SPEED * (0.50F + (float) random()) - particle.getMass());
                        }
                    }
                }

                particle.setSpeedY(particle.getSpeedY() +
                        deltaTime * (this.getGravity() - particle.getMass() * AIR_FRICTION_COEFFICIENT));

                if (abs(particle.getSpeedY()) > MAXIMUM_SPEED) {
                    particle.setSpeedY(signum(particle.getSpeedY()) * MAXIMUM_SPEED);
                }
            }
            // Apply temp - TODO finish
            //Integer t = particle.getTemp();
            //particle.setTemp(t);
        }
    }

    /**
     *
     * @param deltaTime
     * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
     */
    private void updatePositions(final float deltaTime) {
        for (final ParticleCellularImplementation particle : this.getParticles()) {
            if (particle.getType().isMobile()) {
                final float dX = signum(particle.getSpeedX());
                final float dY = signum(particle.getSpeedY());
                final float deltaX = particle.getSpeedX() * deltaTime;
                final float deltaY = particle.getSpeedY() * deltaTime;
                final float oldX = particle.getX();
                final float oldY = particle.getY();
                final float newX = max(0F, min(this.getWidth() - 1F, oldX + deltaX));
                final float newY = max(0F, min(this.getHeight() - 1F, oldY + deltaY));

                if (oldX == newX || ((int) oldX != (int) newX && !this.isCellTraversable(oldX + dX, oldY, particle))) {
                    particle.setSpeedX(0F);
                }

                if (oldY == newY || ((int) oldY != (int) newY && !this.isCellTraversable(oldX, oldY + dY, particle))) {
                    particle.setSpeedY(0F);
                }

                this.tryAndMove(particle,
                        particle.getSpeedX() == 0F ? oldX : newX,
                        particle.getSpeedY() == 0F ? oldY : newY);
            }
        }
    }

    private void fillCells() {
        for (final ParticleCellularImplementation particle : this.getParticles()) {
            this.setCell(particle.getX(), particle.getY(), particle);
        }
    }

    /**
     * This method returns <code>this.borderParticle</code> if the coordinates do not locate a valid cell.
     * @param x
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param y
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    private ParticleCellularImplementation getParticle(final float x, final float y) {
        return this.isInGrid(x, y) ? this.cells[(int) x][(int) y] : this.borderParticle;
    }

    /**
     *
     * @return
     * <br>A non-null value
     * <br>A reference
     */
    @SuppressWarnings("unchecked")
    private Iterable<ParticleCellularImplementation> getParticles() {
        return (Iterable) this;
    }

    /**
     *
     * @return
     * <br>Range: <code>[1 .. Integer.MAX_VALUE]</code>
     */
    private int getHeight() {
        return this.cells != null ? this.cells[0].length : Integer.MAX_VALUE;
    }

    /**
     *
     * @return
     * <br>Range: <code>[1 .. Integer.MAX_VALUE]</code>
     */
    private int getWidth() {
        return this.cells != null ? this.cells.length : Integer.MAX_VALUE;
    }

    private void updateCells() {
        if (this.cells != null && this.getParticleCount() != this.lastParticleCount) {
            for (int x = 0; x < this.cells.length; ++x) {
                Arrays.fill(this.cells[x], null);
            }

            this.fillCells();

            this.lastParticleCount = this.getParticleCount();
        }
    }

    /**
     *
     * @param particle
     * <br>Should not be null
     * <br>Input-output parameter
     * <br>Reference parameter
     * @param newX
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param newY
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     */
    private void tryAndMove(final ParticleCellularImplementation particle, final float newX, final float newY) {
        particle.setTargetLocation(newX, newY);

        final ParticleCellularImplementation target = this.getParticle(newX, newY);

        if (target == null) {
            particle.processReplacementCandidate(particle);
        } else {
            target.processReplacementCandidate(particle);
        }
    }

    /**
     *
     * @param x
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param y
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param particle
     * <br>Should not be null
     * @return
     */
    private boolean isCellTraversable(final float x, final float y, final Particle particle) {
        return this.isInGrid(x, y) && (this.cells[(int) x][(int) y] == null ||
                this.cells[(int) x][(int) y].getType().getMass() < particle.getType().getMass());
    }

    /**
     *
     * @param x
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @param y
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
     * @return
     */
    private boolean isInGrid(final float x, final float y) {
        return this.cells != null && 0F <= x && x < this.cells.length && 0F <= y && y < this.cells[0].length;
    }

    /**
     *
     * @author codistmonk (creation 2010-05-06)
     */
    private final class ParticleCellularImplementation extends ParticleDefaultImplementation {

        private final long id;

        private ParticleCellularImplementation replacementCandidate;

        private float targetX;

        private float targetY;

        /**
         *
         * @param type
         * <br>Should not be null
         * @param x
         * <br>Should not be null
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         * @param y
         * <br>Should not be null
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         * @param speedX
         * <br>Should not be null
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         * @param speedY
         * <br>Should not be null
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         */
        public ParticleCellularImplementation(final Type type, final float x, final float y,
                final float speedX, final float speedY, final Integer temp) {
            super(type, x, y, speedX, speedY, temp);
            this.id = ParticleEngine.this.generateNewId();
        }

        public ParticleCellularImplementation() {
            this(Type.values()[0], 0F, 0F, 0F, 0F, 0);
        }

        /**
         *
         * @param particle
         * <br>Should not be null
         * <br>Input-output parameter
         * <br>Reference parameter
         */
        public final void processReplacementCandidate(final ParticleCellularImplementation particle) {
            assert this == particle || (int) particle.targetX == (int) this.getX() : this + " " + particle;
            assert this == particle || (int) particle.targetY == (int) this.getY() : this + " " + particle;

            if (!this.getType().isMobile()) {
                return;
            }

            if (particle.getType().getMass() > this.getType().getMass()) {
                if (particle.replacementCandidate != null) {
                    this.replacementCandidate = particle.replacementCandidate;
                }

                particle.replacementCandidate = null;

                this.setTargetLocation(particle.getX(), particle.getY());
                this.move();
                particle.updateNewLocation(particle.targetX, particle.targetY);
            } else if (particle.id < this.id) {
                this.replacementCandidate = particle;
            } else if (particle.id == this.id) {
                this.move();
            }
        }

        /**
         *
         * @param targetX
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         * @param targetY
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         */
        public final void setTargetLocation(final float targetX, final float targetY) {
            this.targetX = targetX;
            this.targetY = targetY;
        }

        private void move() {
            if ((int) this.targetX != this.getX() || (int) this.targetY != this.getY()) {
                this.updateOldAndNewLocation(this.targetX, this.targetY);

                if (this.replacementCandidate != null) {
                    final ParticleCellularImplementation replacement = this.replacementCandidate;

                    this.replacementCandidate = null;

                    replacement.move();
                }
            } else {
                this.updateOldAndNewLocation(this.targetX, this.targetY);
            }
        }

        /**
         *
         * @param newX
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         * @param newY
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         */
        private void updateOldAndNewLocation(final float newX, final float newY) {
            ParticleEngine.this.setCell(this.getX(), this.getY(), null);

            this.updateNewLocation(newX, newY);
        }

        /**
         *
         * @param newX
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         * @param newY
         * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
         */
        private void updateNewLocation(final float newX, final float newY) {
            this.setX(newX);
            this.setY(newY);

            ParticleEngine.this.setCell(this.getX(), this.getY(), this);
        }

    }

    private static final float MAXIMUM_SPEED = 25F;

    private static final float AIR_FRICTION_COEFFICIENT = 25F;

    public static final float DEFAULT_GRAVITY = -8F;

}
