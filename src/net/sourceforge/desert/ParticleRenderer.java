package net.sourceforge.desert;

import java.awt.image.BufferedImage;

/**
 *
 * @author codistmonk (creation 2010-04-17)
 */
public class ParticleRenderer {

    private final int[] colorComponents = new int[4];

    private BufferedImage buffer;

    /**
     *
     * @param particle
     * <br>Not null
     */
    public final void draw(final Particle particle) {
        if (this.getBuffer() == null) {
            return;
        }

        final int x = (int) particle.getX();
        final int y = this.getBuffer().getHeight() - (int) particle.getY() - 1;

        this.getBuffer().getRaster().setPixel(x, y, this.getColorComponents(particle));
    }

    /**
     *
     * @return
     * <br>Maybe null
     * <br>Shared
     */
    public final BufferedImage getBuffer() {
        return this.buffer;
    }

    /**
     *
     * @param buffer
     * <br>Maybe null
     * <br>Shared
     */
    public final void setBuffer(final BufferedImage buffer) {
        this.buffer = buffer;
    }

    /**
     *
     * @param particle
     * <br>Not null
     * @return
     * <br>Not null
     * <br>Shared
     */
    private int[] getColorComponents(final Particle particle) {
        this.colorComponents[0] = particle.getType().getColor().getRed();
        this.colorComponents[1] = particle.getType().getColor().getGreen();
        this.colorComponents[2] = particle.getType().getColor().getBlue();
        this.colorComponents[3] = particle.getType().getColor().getAlpha();

        return this.colorComponents;
    }

}
