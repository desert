/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert;

import java.awt.Color;

/**
 *
 * @author codistmonk (creation 2010-04-17)
 */
public interface Particle {

    /**
     *
     * @return
     * <br>A non-null value
     */
    public abstract Type getType();

    /**
     *
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public abstract float getX();

    /**
     *
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public abstract float getY();

    /**
     *
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public abstract float getSpeedX();

    /**
     *
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public abstract float getSpeedY();

    /**
     * @return
     * <br>Range: <code>]Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY[</code>
     */
    public abstract float getMass();

    public abstract Integer getTemp();

    public abstract Integer setTemp(Integer temp);

    /**
     *
     * @author codistmonk (creation 2010-04-17)
     */
    public static enum Type {

        IMMOBILE(IMMOBILE_MASS, DEFAULT_RADIUS, Color.DARK_GRAY, 0),

        ROCK(12.5F, DEFAULT_RADIUS, Color.LIGHT_GRAY, 0),

        SAND(2F, DEFAULT_RADIUS, Color.YELLOW, 0),

        WATER(2.5F, DEFAULT_RADIUS, Color.BLUE, 10),

        SALT(1F, DEFAULT_RADIUS, Color.WHITE, 2),

        LAVA(4.3f, DEFAULT_RADIUS+3, Color.RED, 300),

        METAL(5F, DEFAULT_RADIUS, Color.GRAY, 0),

        GLASS(IMMOBILE_MASS, DEFAULT_RADIUS, Color.BLACK, 0), // as in powder game

        GRASS(IMMOBILE_MASS, DEFAULT_RADIUS, Color.GREEN, 0),

        OIL(2.5F, DEFAULT_RADIUS, Color.decode("#7D343D"), 3),

        ZINC(7.14F, DEFAULT_RADIUS, Color.decode("#414961"), 3),

        GAS(-1.0F, DEFAULT_RADIUS, Color.decode("#996D72"), 2);

        private final float mass;

        private final float radius;

        private final Color color;

        private Integer temp;

        /**
         *
         * @param mass
         * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
         * @param radius
         * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
         * @param color
         * <br>Can be null
         * <br>Reference parameter
         */
        private Type(final float mass, final float radius, final Color color, final Integer temp) {
            this.mass = mass;
            this.radius = radius;
            this.color = color;
            this.temp = temp;
        }

        /**
         *
         * @return true if the mass is non-null and finite
         */
        public final boolean isMobile() {
            return this.getMass() != 0F && !Float.isInfinite(this.getMass());
        }

        /**
         *
         * @return
         * <br>A possibly null value
         * <br>A reference
         */
        public final Color getColor() {
            return this.color;
        }

        /**
         *
         * @return
         * <br>Range: <code>[Float.NEGATIVE_INFINITY .. Float.POSITIVE_INFINITY]</code>
         */
        public final float getMass() {
            return this.mass;
        }

        /**
         *
         * @return
         * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
         */
        public float getRadius() {
            return this.radius;
        }

        /**
         *
         * @return
         * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
         */
        public Integer getTemp() {
            return this.temp;
        }

        public Integer setTemp(Integer t) {
            this.temp = t;
            return this.temp;
        }
    }

    public static final float DEFAULT_RADIUS = 0.49F;

    public static final float IMMOBILE_MASS = Float.POSITIVE_INFINITY;

}
