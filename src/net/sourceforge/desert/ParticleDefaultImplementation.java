/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert;

/**
 * 
 * @author codistmonk (creation 2010-04-17)
 */
public class ParticleDefaultImplementation implements Particle {

    private Type type;

    private float x;

    private float y;

    private float speedX;

    private float speedY;

    private Integer temp;

    public ParticleDefaultImplementation() {
        this(Type.values()[0], 0F, 0F, 0F, 0F, 0);
    }

    /**
     *
     * @param type
     * <br>Should not be null
     * @param x
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     * @param y
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     * @param speedX
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     * @param speedY
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     */
    public ParticleDefaultImplementation(final Type type, final float x, final float y, final float speedX, final float speedY, final Integer temp) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.speedX = speedX;
        this.speedY = speedY;
        this.temp = temp;
    }

    @Override
    public final float getSpeedX() {
        return speedX;
    }

    /**
     *
     * @param speedX
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     */
    public final void setSpeedX(final float speedX) {
        this.speedX = speedX;
    }

    @Override
    public final float getSpeedY() {
        return this.speedY;
    }

    /**
     *
     * @param speedY
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     */
    public final void setSpeedY(final float speedY) {
        this.speedY = speedY;
    }

    @Override
    public final Type getType() {
        return this.type;
    }

    /**
     *
     * @param type
     * <br>Should not be null
     */
    public final void setType(final Type type) {
        this.type = type;
    }

    @Override
    public final float getX() {
        return this.x;
    }

    /**
     *
     * @param x
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     */
    public final void setX(final float x) {
        this.x = x;
    }

    @Override
    public final float getY() {
        return this.y;
    }

    /**
     *
     * @param y
     * <br>Should not be null
     * <br>Range: <code>]float.NEGATIVE_INFINITY .. float.POSITIVE_INFINITY[</code>
     */
    public final void setY(final float y) {
        this.y = y;
    }

    /**
     *
     * @return
     * <br>Range: <code>[Float.NEGATIVE_INFINITY .. float.POSITIVE_INFINIT]</code>
     */
    @Override
    public final float getMass() {
        return this.getType().getMass();
    }
    
    @Override
    public String toString() {
        return "(" + this.getType() + ", " + this.getX() + ", " + this.getY() + ", " + this.getSpeedX() + ", " + this.getSpeedY() + ")";
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Particle.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Particle other = (Particle) obj;
        if (this.getType() != other.getType() && (this.getType() == null || !this.getType().equals(other.getType()))) {
            return false;
        }
        if (this.getX() != other.getX()) {
            return false;
        }
        if (this.getY() != other.getY()) {
            return false;
        }
        if (this.getSpeedX() != other.getSpeedX()) {
            return false;
        }
        if (this.getSpeedY() != other.getSpeedY()) {
            return false;
        }
        return true;
    }

    @Override
    public final int hashCode() {
        int hash = 47 * 3 + (this.type != null ? this.type.hashCode() : 0);
        
        return hash;
    }

    public Integer getTemp() {
        return this.temp;
    }

    public Integer setTemp(Integer temp) {
        this.temp = temp;
        return temp;
    }

}
