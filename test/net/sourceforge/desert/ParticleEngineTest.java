/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert;

import static org.junit.Assert.*;

import java.awt.Dimension;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

/**
 * The tests involving position or speed check inequalities rather than equalities.
 * <br>This way, engines don't have to be scientifically correct; if it looks good then it's good enough.
 * @author codistmonk (creation 2010-04-17)
 */
public final class ParticleEngineTest {

    @Test
    public final void testSetBoundsAndAddParticlesThenRemoveAllThenUpdate() {
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setBoardSize(new Dimension());

        assertNotNull(particleEngine.getBoardSize());
        assertEquals(0, particleEngine.getParticleCount());

        particleEngine.addParticle(IMMOBILE_TYPE, 0F, 0F, 0F, 0F, 0F, 0);

        assertEquals(1, particleEngine.getParticleCount());

        particleEngine.addParticle(MOBILE_TYPE_1, 0F, 0F, 0F, 0F, 0F, 0);

        assertEquals(2, particleEngine.getParticleCount());

        particleEngine.removeAllParticles();

        assertEquals(0, particleEngine.getParticleCount());
        assertNotNull(particleEngine.getBoardSize());

        update(particleEngine, SIMULATION_STEP, SIMULATION_STEP);
    }

    @Test
    public final void testImmobileParticleNotMovedByGravity() {
        final float x = 2F;
        final float y = 3F;
        final float deltaTime = 5F;
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setGravity(ParticleEngine.DEFAULT_GRAVITY);
        particleEngine.setBoardSize(null);
        particleEngine.addParticle(IMMOBILE_TYPE, x, y, 0F, 0F, 0F, 0);
        update(particleEngine, deltaTime, SIMULATION_STEP);

        final Particle particle = particleEngine.iterator().next();

        assertEquals(IMMOBILE_TYPE, particle.getType());
        assertEquals(x, particle.getX(), DELTA);
        assertEquals(y, particle.getY(), DELTA);
        assertEquals(0F, particle.getSpeedX(), DELTA);
        assertEquals(0F, particle.getSpeedY(), DELTA);
    }

    @Test
    public final void testCountThenSetBoundsThenCountThenRemoveBoundsThenCount() {
        final ParticleEngine particleEngine = this.createParticleEngine();

        assertEquals(0, particleEngine.getParticleCount());

        particleEngine.setBoardSize(new Dimension());

        assertEquals(0, particleEngine.getParticleCount());

        particleEngine.setBoardSize(null);

        assertEquals(0, particleEngine.getParticleCount());
    }

    @Test
    public final void testUpdateWithBottomBound() {
        final Integer width = 20;
        final Integer height = 10;
        final float x = 10F;
        final float y = 5F;
        final float speedX = 1F;
        final float speedY = 5F; // Go downward in screen coordinates
        final float deltaTime = 2F;
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setGravity(0F);
        particleEngine.setBoardSize(new Dimension(width, height));
        particleEngine.addParticle(MOBILE_TYPE_1, x, y, speedX, speedY, 0F, 0);
        update(particleEngine, deltaTime, SIMULATION_STEP);

        final Particle particle = particleEngine.iterator().next();

        assertTrue(particle.getY() + " >= " + height, particle.getY() < height);
    }

    @Test
    public final void testUpdateWithTopBound() {
        final Integer width = 20;
        final Integer height = 10;
        final float x = 10F;
        final float y = 5F;
        final float speedX = 1F;
        final float speedY = -5F; // Go upward in screen coordinates
        final float deltaTime = 2F;
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setGravity(0F);
        particleEngine.setBoardSize(new Dimension(width, height));
        particleEngine.addParticle(MOBILE_TYPE_1, x, y, speedX, speedY, 0F, 0);
        update(particleEngine, deltaTime, SIMULATION_STEP);
        
        final Particle particle = particleEngine.iterator().next();

        assertTrue(particle.getY() + " < 0", particle.getY() >= 0);
    }

    @Test
    public final void testUpdateWithLeftBound() {
        final Integer width = 20;
        final Integer height = 10;
        final float x = 10F;
        final float y = 4F;
        final float speedX = -10F;
        final float speedY = 1F;
        final float deltaTime = 2F;
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setGravity(0F);
        particleEngine.setBoardSize(new Dimension(width, height));
        particleEngine.addParticle(MOBILE_TYPE_1, x, y, speedX, speedY, 0F, 0);
        update(particleEngine, deltaTime, SIMULATION_STEP);

        final Particle particle = particleEngine.iterator().next();

        assertTrue(particle.getX() + " < 0", particle.getX() >= 0);
    }

    @Test
    public final void testUpdateWithRightBound() {
        final Integer width = 20;
        final Integer height = 10;
        final float x = 10F;
        final float y = 4F;
        final float speedX = 10F;
        final float speedY = 1F;
        final float deltaTime = 2F;
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setGravity(0F);
        particleEngine.setBoardSize(new Dimension(width, height));
        particleEngine.addParticle(MOBILE_TYPE_1, x, y, speedX, speedY, 0F, 0);
        update(particleEngine, deltaTime, SIMULATION_STEP);

        final Particle particle = particleEngine.iterator().next();

        assertTrue(particle.getX() + " >= " + width, particle.getX() < width);
    }

    @Test
    public final void testSetThenGetGravity() {
        final ParticleEngine particleEngine = this.createParticleEngine();
        final float gravity = -42F;

        particleEngine.setGravity(gravity);
        
        assertEquals(gravity, particleEngine.getGravity(), DELTA);
    }

    @Test
    public final void testSetThenGetEmptyBoardSize() {
        final ParticleEngine particleEngine = this.createParticleEngine();
        final Dimension size = new Dimension(0, 0);

        particleEngine.setBoardSize(size);
        
        assertEquals(size, particleEngine.getBoardSize());
    }

    @Test
    public final void testSetThenGetNonemptyBoardSize() {
        final ParticleEngine particleEngine = this.createParticleEngine();
        final Dimension size = new Dimension(640, 480);

        particleEngine.setBoardSize(size);

        assertEquals(size, particleEngine.getBoardSize());
    }

    @Test
    public final void testAddParticlesAndCountWithoutBounds() {
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setBoardSize(null);

        assertEquals(0, particleEngine.getParticleCount());

        particleEngine.addParticle(Particle.Type.values()[0], 0F, 0F, 0F, 0F, 0F, 0);

        assertEquals(1, particleEngine.getParticleCount());

        particleEngine.addParticle(Particle.Type.values()[0], 0F, 0F, 0F, 0F, 0F, 0);

        assertEquals(2, particleEngine.getParticleCount());
    }

    @Test
    public final void testAddParticlesAndCountWithBounds() {
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setBoardSize(new Dimension());

        assertEquals(0, particleEngine.getParticleCount());

        particleEngine.addParticle(Particle.Type.values()[0], 0F, 0F, 0F, 0F, 0F, 0);

        assertEquals(1, particleEngine.getParticleCount());

        particleEngine.addParticle(Particle.Type.values()[0], 0F, 0F, 0F, 0F, 0F, 0);

        assertEquals(2, particleEngine.getParticleCount());
    }

    @Test
    public final void testUpdate() {
        final ParticleEngine particleEngine = this.createParticleEngine();
        final ParticleDefaultImplementation particle1 = new ParticleDefaultImplementation(
                MOBILE_TYPE_1, 2F, 3F, 5F, 7F, 0);
        final ParticleDefaultImplementation particle2 = new ParticleDefaultImplementation(
                MOBILE_TYPE_2, 11F, 13F, 17F, 19F, 0);

        particleEngine.setBoardSize(new Dimension(100, 100));
        particleEngine.setGravity(0F);

        addParticle(particleEngine, particle1);
        addParticle(particleEngine, particle2);

        update(particleEngine, 1F, SIMULATION_STEP);

        final Iterator<Particle> particleIterator = particleEngine.iterator();
        
        assertTrue(particleIterator.next().getY() > particle1.getY());
        assertTrue(particleIterator.next().getY() > particle2.getY());
    }

    @Test
    public final void testIteratorWithoutBounds() {
        final ParticleEngine particleEngine = this.createParticleEngine();
        final ParticleDefaultImplementation particle1 = new ParticleDefaultImplementation(
                MOBILE_TYPE_1, 2F, 3F, 5F, 7F, 0);
        final ParticleDefaultImplementation particle2 = new ParticleDefaultImplementation(
                MOBILE_TYPE_2, 11F, 13F, 17F, 19F, 0);

        particleEngine.setBoardSize(null);
        addParticle(particleEngine, particle1);
        addParticle(particleEngine, particle2);

        final Iterator<Particle> particleIterator = particleEngine.iterator();
        final Set<Particle> expectedParticles = new HashSet<Particle>();

        expectedParticles.add(particle1);
        expectedParticles.add(particle2);

        for (int i = expectedParticles.size(); i > 0; --i) {
            assertTrue("expected at least " + (i + 1) + " particles", particleIterator.hasNext());

            final Particle particle = particleIterator.next();

            assertTrue("unexpected particle " + particle, expectedParticles.contains(particle));

            expectedParticles.remove(particle);
        }
        assertTrue("missing particles " + expectedParticles, expectedParticles.isEmpty());
        assertFalse(particleIterator.hasNext());
    }

    @Test
    public final void testIteratorWithBounds() {
        final ParticleEngine particleEngine = this.createParticleEngine();
        final ParticleDefaultImplementation particle1 = new ParticleDefaultImplementation(
                MOBILE_TYPE_1, 2F, 3F, 5F, 7F, 0);
        final ParticleDefaultImplementation particle2 = new ParticleDefaultImplementation(
                MOBILE_TYPE_2, 11F, 13F, 17F, 19F, 0);

        particleEngine.setBoardSize(new Dimension());
        addParticle(particleEngine, particle1);
        addParticle(particleEngine, particle2);

        final Iterator<Particle> particleIterator = particleEngine.iterator();
        final Set<Particle> expectedParticles = new HashSet<Particle>();

        expectedParticles.add(particle1);
        expectedParticles.add(particle2);

        for (int i = expectedParticles.size(); i > 0; --i) {
            assertTrue("expected at least " + (i + 1) + " particles", particleIterator.hasNext());

            final Particle particle = particleIterator.next();

            assertTrue("unexpected particle " + particle, expectedParticles.contains(particle));

            expectedParticles.remove(particle);
        }
        assertTrue("missing particles " + expectedParticles, expectedParticles.isEmpty());
        assertFalse(particleIterator.hasNext());
    }

    @Test
    public final void testIteratorRemoveWithoutBounds() {
        final ParticleEngine particleEngine = this.createParticleEngine();

        particleEngine.setBoardSize(null);
        particleEngine.addParticle(IMMOBILE_TYPE, 0F, 0F, 0F, 0F, 0F, 0);
        particleEngine.addParticle(MOBILE_TYPE_1, 0F, 0F, 0F, 0F, 0F, 0);
        particleEngine.addParticle(MOBILE_TYPE_2, 0F, 0F, 0F, 0F, 0F, 0);

        {
            final Set<Particle.Type> types = new HashSet<Particle.Type>();

            for (final Particle particle : particleEngine) {
                types.add(particle.getType());
            }

            assertTrue(types.contains(IMMOBILE_TYPE));
            assertTrue(types.contains(MOBILE_TYPE_1));
            assertTrue(types.contains(MOBILE_TYPE_2));
            assertEquals(3, particleEngine.getParticleCount());
        }

        for (final Iterator<Particle> iterator = particleEngine.iterator(); iterator.hasNext();) {
            if (iterator.next().getType().equals(MOBILE_TYPE_1)) {
                iterator.remove();
            }
        }
        
        {
            final Set<Particle.Type> types = new HashSet<Particle.Type>();

            for (final Particle particle : particleEngine) {
                types.add(particle.getType());
            }

            assertTrue(types.contains(IMMOBILE_TYPE));
            assertFalse(types.contains(MOBILE_TYPE_1));
            assertTrue(types.contains(MOBILE_TYPE_2));
            assertEquals(2, particleEngine.getParticleCount());
        }
    }

    /**
     *
     * @return
     * <br>A new value
     * <br>A non-null value
     */
    protected final ParticleEngine createParticleEngine() {
        return new ParticleEngine();
    }

    protected static final float DELTA = 1e-12F;

    protected static final float SIMULATION_STEP = 1.0F / 60.0F;

    protected static final Particle.Type IMMOBILE_TYPE = Particle.Type.IMMOBILE;

    protected static final Particle.Type MOBILE_TYPE_1 = Particle.Type.SAND;

    protected static final Particle.Type MOBILE_TYPE_2 = Particle.Type.WATER;

    /**
     *
     * @param particleEngine
     * <br>Should not be null
     * @param particle
     * <br>Should not be null
     */
    protected static final void addParticle(final ParticleEngine particleEngine, final Particle particle) {
        particleEngine.addParticle(particle.getType(), particle.getX(), particle.getY(), particle.getSpeedX(), particle.getSpeedY(), particle.getMass(), particle.getTemp());
    }

    /**
     *
     * @param particleEngine
     * <br>Should not be null
     * @param duration
     * <br>Range: <code>[0F .. Float.POSITIVE_INFINITY[</code>
     * @param deltaTime
     * <br>Range: <code>]0F .. Float.POSITIVE_INFINITY[</code>
     */
    protected static final void update(final ParticleEngine particleEngine, final float duration, final float deltaTime) {
        float time = 0F;
        
        while (time < duration) {
            particleEngine.update(deltaTime);
            
            time += deltaTime;
        }
    }

}