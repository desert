/*
 * Copyright (c) 2010 The Desert team
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package net.sourceforge.desert.ui;

import net.sourceforge.desert.ui.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author codistmonk (creation 2010-04-15)
 */
public class TimerTest {

    @Test
    public final void testDeltaTime() throws InterruptedException {
        final int framerate = 2;
        final float expectedDeltaTime = 1F / framerate;
        final Timer timer = new Timer(framerate);
        final float[] deltaTimeBuffer = new float[1];
        final Semaphore semaphore = new Semaphore(0);

        timer.setRepeats(false);
        
        timer.addActionListener(new ActionListener() {

            @Override
            public final void actionPerformed(final ActionEvent event) {
                deltaTimeBuffer[0] = ((Timer.Event) event).getDeltaTime();
                semaphore.release();
            }

        });

        timer.start();

        assertTrue(semaphore.tryAcquire(TIMEOUT, TimeUnit.SECONDS));

        assertEquals(expectedDeltaTime, deltaTimeBuffer[0], 1e-3F);
    }

    private static final long TIMEOUT = 5L;

}